Disclaimer

Some content on my personal web directory (this site) is for my
personal use only, and may not be legally distributable to other
parties. I actively try to keep these properly secured so others
cannot download them, but it is possible this might not work for
whatever reason. If you are able to download anything that I
shouldn't be distributing, please contact me (nikoloskid@pm.me)
and I will gladly make sure access to it is restricted properly.

By downloading anything from my personal web directory here, you
assume responsibility for ensuring the copy is in compliance with
the laws of your jurisdiction, my jurisdiction (Slovenia; North Macedonia), and
my server's jurisdiction (Germany). If you do not agree to these
terms, you are not authorized to access this website. Accessing
this website implies you have accepted this agreement as a binding
contract.

