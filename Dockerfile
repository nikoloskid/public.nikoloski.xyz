FROM nginx:1.25.0-alpine

# Configuration
COPY nginx.conf /etc/nginx/nginx.conf

# Content
COPY ./*.html /usr/share/nginx/html/
COPY ./dir/cv/* /usr/share/nginx/html/cv/
COPY ./dir/keys/* /usr/share/nginx/html/keys/

# Copy README and robots.txt
COPY ./dir/README.txt /usr/share/nginx/html/
COPY ./dir/robots.txt /usr/share/nginx/html/

# Create some useful directories
RUN mkdir -p /usr/share/nginx/html/pdfs/
RUN mkdir -p /usr/share/nginx/html/random/
RUN mkdir -p /usr/share/nginx/html/releases/
RUN mkdir -p /usr/share/nginx/html/temp/

EXPOSE 80